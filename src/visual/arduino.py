import serial
import re
import pygame

#pygame stuff
pygame.init()
screen=pygame.display.set_mode([640,480])

#serial stuff
ser = serial.Serial('/dev/ttyACM0', 9600)
tabela = {'x': re.compile('x([0-9]+)'),
		  'y': re.compile('y([0-9]+)'),
		  'z': re.compile('z([0-9]+)')}

x = 0
y = 0
z = 0

while True:
	for event in pygame.event.get():
		if event.type==pygame.QUIT:
			sys.exit()

	#read the string of arduino
	s = ser.readline()
	s = str(s)
	s = s.replace("b'", "")
	s = s.replace("\\n'", "")

	if re.search(tabela['x'], s):
		x = re.search(tabela['x'], s).group(1)

	if re.search(tabela['y'], s):
		y = re.search(tabela['y'], s).group(1)

	if re.search(tabela['z'], s):
		z = re.search(tabela['z'], s).group(1)

	print('x: ', x, ' y: ', y, ' z: ', z)

	#fill the screen and draw a rect with the positions x,y and z
	screen.fill([0,0,255])
	pygame.draw.rect(screen, (0, 255, 0), (int(x), int(y), int(z), int(z)), 0)
	pygame.display.flip()