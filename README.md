# COMUNICÃO ARDUINO E MÓDULO DE ACELERÔMETRO

Trabalho feito por **Emanuel Ozorio Dias** e **Adão Diego Dutra** para a disciplina de comunicação de dados.

## OBJETIVO

O objetivo é utilizar arduino, juntamente com um acelerômetro para demonstrar
graficamente no console as posições de inclinação dos eixos X, Y e Z. Com esse projeto buscamos maior conhecimentos sobre arduino sua plataforma de comunicação em **C** e uma programação em **Python** para demonstrar graficamente e simultaneamente na tela os valores dos eixos de inclinação obtidos com o arduino. 

## COMPONENTES
- 1 arduino UNO
- 1 modulo de acelerômetro MMA7361
- vários Jumpers

## PROJETO
A parte de desenvolvimento pode ser dividida em duas partes. Na primeira parte foi realizada a ligação do arduino com o acelerometro e feita a parte de recolhimento e tratamento dos dados. Na segunda, foi feito uma pequena aplicação na linguagem **Python** que pega esses dados já tratados e, utilizando a biblioteca **Pygame**, demonstra graficamente em forma de um retangulo na tela, as inclinações de cada eixo. Sendo o que os eixos X e Y foram usados para o movimento do retangulo e o eixo Z modifica o tamanho do retangulo.

### Ligação do módulo ao arduino
|Pino módulo|Função|Ligação ao Arduino|
|-----------|------|------------------|
|getXAccel|voltagem de saída eixo X|A0|
|Y|voltagem de saída eixo Y|A1|
|Z|voltagem de saída eixo Z|A2|
|3V3|Alimentação 3.3 Volts|Ligar ao 3V3 do arduíno (Melhor presisão)|
|5V|Alimentação 5 Volts|Ligar ao 5V do arduino|
|ST|Self Tests|Pino 12|
|GS|Seleção do modo de sensibilidade|Pino 10|
|0G|Detecção de queda livre linear|Pino 11|
|SL|Habilita o modo sleep|Pino 13|
|GND|Ground|GND|

![ligacao](https://gitlab.com/EmanuelOzorio/comunicacao-dados/raw/729085e7fc2f7f96a604eac9d5dfffd3cc4158cb/img/ligacao-arduino.jpg)

### Placa montada e código executando

![montagem](https://gitlab.com/EmanuelOzorio/comunicacao-dados/raw/729085e7fc2f7f96a604eac9d5dfffd3cc4158cb/img/arduino_montagem.jpg)

![executando](https://gitlab.com/EmanuelOzorio/comunicacao-dados/raw/729085e7fc2f7f96a604eac9d5dfffd3cc4158cb/img/executando.png)

## CÓDIGO FONTE
### Arduino

```cpp
#include <AcceleroMMA7361.h> //Carrega a biblioteca do MMA7361  

AcceleroMMA7361 accelero;
int x;
int y;
int z;

void setup()
{
  Serial.begin(9600);
  accelero.begin(13, 12, 11, 10, A0, A1, A2);
  //Seta a voltagem de referencia AREF como 3.3V
  accelero.setARefVoltage(3.3);
  //Serial.print("\tG*10^-2");
  //Seta a sensibilidade (Pino GS) para +/-6G    
  accelero.setSensitivity(LOW);
  accelero.calibrate();
}

void loop()
{
  x = accelero.getXAccel(); //Obtem o valor do eixo X
  y = accelero.getYAccel(); //Obtem o valor do eixo Y
  z = accelero.getZAccel(); //Obtem o valor do eixo Z
  Serial.print('\nx:');
  Serial.print(x);
  Serial.print('\ny:');
  Serial.print(y);
  Serial.print('\nz:');
  Serial.print(z);
  delay(100);
}
```

### Visual (Em PYTHON)

```python
import serial
import re
import pygame

## Pygame
pygame.init() # Inicia a lib
screen=pygame.display.set_mode([640,480]) # Declara a tela

## Serial
## Inicia a lib na porta onde o arduino esta plugado
ser = serial.Serial('/dev/ttyACM0', 9600)
## Tabela com o regex para tratar o retorno do arduino
tabela = {'x': re.compile('x([0-9]+)'),
      'y': re.compile('y([0-9]+)'),
      'z': re.compile('z([0-9]+)')}

x = 0
y = 0
z = 0

## Loop principal
while True:
  # Evento de saida
  for event in pygame.event.get():
    if event.type==pygame.QUIT:
      sys.exit()

  # Le a string retornada pelo arduino
  s = ser.readline()
  s = str(s)
  s = s.replace("b'", "")
  s = s.replace("\\n'", "")

  # Trata a string separando o X, Y e Z
  if re.search(tabela['x'], s):
    x = re.search(tabela['x'], s).group(1)

  if re.search(tabela['y'], s):
    y = re.search(tabela['y'], s).group(1)

  if re.search(tabela['z'], s):
    z = re.search(tabela['z'], s).group(1)

  print('x: ', x, ' y: ', y, ' z: ', z)

  # Limpa a tela e desenha um retangulo com os 
  # valores X, Y e Z que o arduino retornou
  screen.fill([0,0,255])
  pygame.draw.rect(screen, (0, 255, 0), (int(x), int(y), int(z), int(z)), 0)
  pygame.display.flip()
```

## RESULTADO
Apesar de encontradas algumas dificuldades na implementação e principalmente no tratamento das informações obtidas do arduino para a transformação desses dados em um objeto visível e de fácil entendimento para o usuário, os resultados obtidos foram positivos. Também foi feita uma pesquisa pelos integrantes da equipe sobre o funcionamento dos eixos do acelerômetro. Referente a parte gráfica feita em Phynton foi mais facilmente implementado devido ao prévio conhecimento dessa linguagem de um dos integrantes da equipe. Importante destacar também a facilidade de integração da plataforma arduino com diferentes linguagens de programação, o que facilita o desenvolvimento de bons projetos como esse de forma rápida e sem muitos erros.

## COMO RODAR O CÓDIGO
Se você estiver utilizando UBUNTU:

- Na IDE do arduino carregar o código encontrado em *src/arduino/arduino.ino*;
- Com o comando ```sudo apt get install``` instalar as bibliotecas *python3*, *python3-pygame*, *python3-re* e *python3-serial*;
- Executar o arquivo *arduino.py*.

## REFERENCIAS E LINKS UTÉIS

- Tutorial de como montar o arduino com o módulo mma7361:
<http://www.arduinoecia.com.br/2013/09/ligando-acelerometro-mma7361-no-arduino.html>

- Documentação da Pygame:
<http://www.pygame.org/docs/>

- Site util para testar e gerar o código de expressões regulares:
<https://regex101.com/>